import unittest
from LogFunc import LogicalGate
from EightBitAdder import EightBitAdder

class EightBitAdderTest(unittest.TestCase):
    def testcase_01(self):
        a = EightBitAdder("Name", True, True, True, True, True, True, True, True,
												False, False, False, False, False, False, False, False,
												True)
        self.assertEqual(a.getSum(0), False, "Class EightBitAdder: Testcase 1 failed")
        self.assertEqual(a.getSum(1), False, "Class EightBitAdder: Testcase 1 failed")
        self.assertEqual(a.getSum(2), False, "Class EightBitAdder: Testcase 1 failed")
        self.assertEqual(a.getSum(3), False, "Class EightBitAdder: Testcase 1 failed")
        self.assertEqual(a.getSum(4), False, "Class EightBitAdder: Testcase 1 failed")
        self.assertEqual(a.getSum(5), False, "Class EightBitAdder: Testcase 1 failed")
        self.assertEqual(a.getSum(6), False, "Class EightBitAdder: Testcase 1 failed")
        self.assertEqual(a.getSum(7), False, "Class EightBitAdder: Testcase 1 failed")
        self.assertEqual(a.getCarry(), True, "Class EightBitAdder: Testcase 1 failed")

    def testcase_02(self):
        a = EightBitAdder("FunnyName", True, False)
        self.assertEqual(a.getCarry(), None, "Class EightBitAdder: Testcase 9 failed")

    def testcase_03(self):
        a = EightBitAdder("FunnyName", True)
        self.assertEqual(a.getSum(), None, "Class EightBitAdder: Testcase 10 failed")

    def testcase_04(self):
        a = EightBitAdder("FunnyName", True, True, True, True)
        self.assertEqual(a.getSum(), None, "Class EightBitAdder: Testcase 11 failed")

    def testcase_05(self):
        a = EightBitAdder("FunnyName", True, True)
        self.assertEqual(a.getCarry(), None, "Class EightBitAdder: Testcase 12 failed")