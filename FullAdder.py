from LogFunc import LogicalGate
from HalfAdder import HalfAdder
from OrGate import OrGate

class FullAdder(LogicalGate):
    def __init__(self, name = None, *bools):
        self.__HA1 = HalfAdder()
        self.__HA2 = HalfAdder()
        self.__OR = OrGate()
        super(FullAdder, self).__init__(name, *bools)

    def _execute(self):
        self.__HA1.setInput(self._input[0], self._input[1])
        self.__HA2.setInput(self.__HA1.getSum(), self._input[2])
        self.__OR.setInput(self.__HA1.getCarry(), self.__HA2.getCarry())

        self._setOutput(self.__HA2.getSum(), self.__OR.getOutput())

    def setInput(self, *bools):
        if len(bools) == 3:
            super(FullAdder, self).setInput(*bools)
        else:
            self._setOutputInvalid()

    def getSum(self):
        return self.getOutput()

    def getCarry(self):
        return self.getOutput(1)