import unittest
from LogFunc import LogicalGate
from OrGate import OrGate

class OrGateTest(unittest.TestCase):
    def testcase_01(self):
        a = OrGate("Name", False, False)
        self.assertEqual(a.getOutput(), False, "Class AndGate: Testcase 1 failed")

    def testcase_02(self):
        a = OrGate("Name", True, False)
        self.assertEqual(a.getOutput(), True, "Class AndGate: Testcase 2 failed")

    def testcase_03(self):
        a = OrGate("Name", False, True)
        self.assertEqual(a.getOutput(), True, "Class AndGate: Testcase 3 failed")

    def testcase_04(self):
        a = OrGate("Name", True, True)
        self.assertEqual(a.getOutput(), True, "Class AndGate: Testcase 4 failed")

    def testcase_05(self):
        a = OrGate("Name", False, False, False, False, False)
        self.assertEqual(a.getOutput(), False, "Class AndGate: Testcase 5 Multiple Inputs Failed")

    def testcase_06(self):
        a = OrGate("Name", True, True, False, True, True)
        self.assertEqual(a.getOutput(), True, "Class AndGate: Testcase 6 Multiple Inputs Failed")

if __name__ == "__main__":
    if __name__ == '__main__':
        unittest.main()