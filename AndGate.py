from LogFunc import LogicalGate

class AndGate(LogicalGate):
    def _execute(self):
        output = True
        for boolVal in self._input:
            if boolVal == False:
                output = False
                break
        self._setOutput(output)


