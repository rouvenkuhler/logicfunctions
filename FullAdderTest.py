import unittest
from LogFunc import LogicalGate
from FullAdder import FullAdder

class FullAdderTest(unittest.TestCase):
    def testcase_01(self):
        a = FullAdder("Name", False, False, False)
        self.assertEqual(a.getSum(), False, "Class FullAdder: Testcase 1 failed")

    def testcase_02(self):
        a = FullAdder("Name", False, False, False)
        self.assertEqual(a.getCarry(), False, "Class FullAdder: Testcase 2 failed")

    def testcase_03(self):
        a = FullAdder("Name", False, True, False)
        self.assertEqual(a.getSum(), True, "Class FullAdder: Testcase 3 failed")

    def testcase_04(self):
        a = FullAdder("Name", False, True, False)
        self.assertEqual(a.getCarry(), False, "Class FullAdder: Testcase 4 failed")

    def testcase_05(self):
        a = FullAdder("Name", True, False, False)
        self.assertEqual(a.getSum(), True, "Class FullAdder: Testcase 5 failed")

    def testcase_06(self):
        a = FullAdder("Name", True, False, False)
        self.assertEqual(a.getCarry(), False, "Class FullAdder: Testcase 6 failed")

    def testcase_07(self):
        a = FullAdder("Name", True, True, False)
        self.assertEqual(a.getSum(), False, "Class FullAdder: Testcase 7 failed")

    def testcase_08(self):
        a = FullAdder("Name", True, True, False)
        self.assertEqual(a.getCarry(), True, "Class FullAdder: Testcase 8 failed")

    def testcase_09(self):
        a = FullAdder("FunnyName", True, False)
        self.assertEqual(a.getCarry(), None, "Class FullAdder: Testcase 9 failed")

    def testcase_10(self):
        a = FullAdder("FunnyName", True)
        self.assertEqual(a.getSum(), None, "Class FullAdder: Testcase 10 failed")

    def testcase_11(self):
        a = FullAdder("FunnyName", True, True, True, True)
        self.assertEqual(a.getSum(), None, "Class FullAdder: Testcase 11 failed")

    def testcase_12(self):
        a = FullAdder("FunnyName", True, True)
        self.assertEqual(a.getCarry(), None, "Class FullAdder: Testcase 12 failed")