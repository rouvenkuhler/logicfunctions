import unittest
from LogFunc import LogicalGate
from HalfAdder import HalfAdder

class HalfAdderTest(unittest.TestCase):
    def testcase_01(self):
        a = HalfAdder("Name", False, False)
        self.assertEqual(a.getSum(), False, "Class AndGate: Testcase 1 failed")

    def testcase_02(self):
        a = HalfAdder("Name", True, False)
        self.assertEqual(a.getSum(), True, "Class AndGate: Testcase 2 failed")

    def testcase_03(self):
        a = HalfAdder("Name", False, True)
        self.assertEqual(a.getSum(), True, "Class AndGate: Testcase 3 failed")

    def testcase_04(self):
        a = HalfAdder("Name", True, True)
        self.assertEqual(a.getSum(), False, "Class AndGate: Testcase 4 failed")

    def testcase_05(self):
        a = HalfAdder("Name", False, False)
        self.assertEqual(a.getCarry(), False, "Class AndGate: Testcase 5 failed")

    def testcase_06(self):
        a = HalfAdder("Name", True, False)
        self.assertEqual(a.getCarry(), False, "Class AndGate: Testcase 6 failed")

    def testcase_07(self):
        a = HalfAdder("Name", False, True)
        self.assertEqual(a.getCarry(), False, "Class AndGate: Testcase 7 failed")

    def testcase_08(self):
        a = HalfAdder("Name", True, True)
        self.assertEqual(a.getCarry(), True, "Class AndGate: Testcase 8 failed")

    def testcase_09(self):
        a = HalfAdder("FunnyName", True)
        self.assertEqual(a.getCarry(), None, "Class AndGate: Testcase 9 failed")

    def testcase_10(self):
        a = HalfAdder("FunnyName", True)
        self.assertEqual(a.getSum(), None, "Class AndGate: Testcase 10 failed")

    def testcase_11(self):
        a = HalfAdder("FunnyName", True, True, True)
        self.assertEqual(a.getSum(), None, "Class AndGate: Testcase 11 failed")

    def testcase_12(self):
        a = HalfAdder("FunnyName", True, True, True)
        self.assertEqual(a.getSum(), None, "Class AndGate: Testcase 12 failed")