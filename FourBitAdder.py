from LogFunc import LogicalGate
from FullAdder import FullAdder

class FourBitAdder(LogicalGate):
	def __init__(self, name = None, *bools):
		self.__carryIn = False

		self.__FA1 = FullAdder()
		self.__FA2 = FullAdder()
		self.__FA3 = FullAdder()
		self.__FA4 = FullAdder()

		super(FourBitAdder, self).__init__(name, *bools)

	def _execute(self):
		self.__FA1.setInput(self.__carryIn, self._input[0], self._input[4])
		self.__FA2.setInput(self.__FA1.getCarry(), self._input[1], self._input[5])
		self.__FA3.setInput(self.__FA2.getCarry(), self._input[2], self._input[6])
		self.__FA4.setInput(self.__FA3.getCarry(), self._input[3], self._input[7])

		self._setOutput(self.__FA1.getSum(),
						self.__FA2.getSum(),
						self.__FA3.getSum(),
						self.__FA4.getSum(),
						self.__FA4.getCarry())

	def setInput(self, *bools):
		if len(bools) == 8:
			super(FourBitAdder, self).setInput(*bools)
		elif len(bools) == 9:
			self.__carryIn = bools[8]
			super(FourBitAdder, self).setInput(*bools)
		else:
			self._setOutputInvalid()

	def getSum(self, getAt = 0):
		if getAt <= 3 and getAt >= 0:
			return self.getOutput(getAt)
		return None

	def getCarry(self):
		return self.getOutput(4)