from LogFunc import LogicalGate

class OrGate(LogicalGate):
    def _execute(self):
        output = False

        for boolVal in self._input:
            if boolVal == True and output == False:
                output = True
                break

        self._setOutput(output)