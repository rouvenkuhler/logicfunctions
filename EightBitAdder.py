from LogFunc import LogicalGate
from FourBitAdder import FourBitAdder

class EightBitAdder(LogicalGate):
	def __init__(self, name = None, *bools):
		self.__carryIn = False
		self.__FourBA1 = FourBitAdder()
		self.__FourBA2 = FourBitAdder()

		super(EightBitAdder, self).__init__(name, *bools)

	def _execute(self):
		self.__FourBA1.setInput(self._input[0],
								self._input[1],
								self._input[2],
								self._input[3],
								self._input[8],
								self._input[9],
								self._input[10],
								self._input[11],
								self.__carryIn)
		self.__FourBA2.setInput(self._input[4],
								self._input[5],
								self._input[6],
								self._input[7],
								self._input[12],
								self._input[13],
								self._input[14],
								self._input[15],
								self.__FourBA1.getCarry())

		self._setOutput(self.__FourBA1.getSum(0),
						self.__FourBA1.getSum(1),
						self.__FourBA1.getSum(2),
						self.__FourBA1.getSum(3),
						self.__FourBA2.getSum(0),
						self.__FourBA2.getSum(1),
						self.__FourBA2.getSum(2),
						self.__FourBA2.getSum(3),
						self.__FourBA2.getCarry())

	def setInput(self, *bools):
		if len(bools) == 16:
			super(EightBitAdder, self).setInput(*bools)
		elif len(bools) == 17:
			self.__carryIn = bools[16]
			super(EightBitAdder, self).setInput(*bools)
		else:
			self._setOutputInvalid()

	def getSum(self, getAt = 0):
		if getAt <= 15 and getAt >= 0:
			return self.getOutput(getAt)
		return None

	def getCarry(self):
		return self.getOutput(8)
