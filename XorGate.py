from LogFunc import LogicalGate

class XorGate(LogicalGate):
    def _execute(self):
        output = False

        for boolVal in self._input:
            if boolVal == True:
                output = not output
        self._setOutput(output)