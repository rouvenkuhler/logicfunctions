from abc import ABC, abstractmethod
import types

class LogicalGate(ABC):
	def __init__(self, name = None, *bools):
		self._input = []
		self.setInput(*bools)

		if name == None:
			self.name = self.__class__.__name__
		else:
			self.name = name # string

	def __str__(self):
		return "[class: " + self.__class__.__name__ + \
				"; name: " + str(self.name) + ";]"

	@abstractmethod
	def _execute(self):
		pass

	def show(self):
		print(str(self._output))
		return

	def setInput(self, *bools):
		for boolVal in bools:
			if not isinstance(boolVal, bool):
				self.__output = None
				return

		self._input = bools
		self._execute()

	def _setOutput(self, *bools):
		self.__output = bools

	def _setOutputInvalid(self):
		self.__output = None

	def getInputList(self):
		return self._input

	def getOutputList(self):
		return self.__output

	def getOutput(self, getAt = 0):
		if not isinstance(self.__output, type(None)):
			return self.__output[getAt]
		return None

	def getInput(self, getAt = 0):
		return self._input[getAt]
