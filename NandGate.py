from LogFunc import LogicalGate

class NandGate(LogicalGate):
    def _execute(self):
        output = False
        for boolVal in self._input:
            if boolVal == False:
                output = True
                break

        self._setOutput(output)