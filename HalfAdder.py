from LogFunc import LogicalGate
from AndGate import AndGate
from XorGate import XorGate

class HalfAdder(LogicalGate):
    def __init__(self, name = None, *bools):
        self.__XOR = XorGate()
        self.__AND = AndGate()
        super(HalfAdder, self).__init__(name, *bools)

    def _execute(self):
        self.__XOR.setInput(self._input[0], self._input[1])
        self.__AND.setInput(self._input[0], self._input[1])

        self._setOutput(self.__XOR.getOutput(), self.__AND.getOutput())

    def setInput(self, *bools):
        if len(bools) == 2:
            super(HalfAdder, self).setInput(*bools)
        else:
            self._setOutputInvalid()

    def getSum(self):
        return self.getOutput()

    def getCarry(self):
        return self.getOutput(1)
