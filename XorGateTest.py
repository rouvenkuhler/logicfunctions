import unittest
from LogFunc import LogicalGate
from XorGate import XorGate

class XorGateTest(unittest.TestCase):
    def testcase_01(self):
        a = XorGate("Name", False, False)
        self.assertEqual(a.getOutput(), False, "Class AndGate: Testcase 1 failed")

    def testcase_02(self):
        a = XorGate("Name", True, False)
        self.assertEqual(a.getOutput(), True, "Class AndGate: Testcase 2 failed")

    def testcase_03(self):
        a = XorGate("Name", False, True)
        self.assertEqual(a.getOutput(), True, "Class AndGate: Testcase 3 failed")

    def testcase_04(self):
        a = XorGate("Name", True, True)
        self.assertEqual(a.getOutput(), False, "Class AndGate: Testcase 4 failed")

    def testcase_05(self):
        a = XorGate("Name", True, False, True, False, False)
        self.assertEqual(a.getOutput(), False, "Class AndGate: Testcase 5 Multiple Inputs Failed")

    def testcase_06(self):
        a = XorGate("Name", True, False, False, False, False)
        self.assertEqual(a.getOutput(), True, "Class AndGate: Testcase 6 Multiple Inputs Failed")

if __name__ == "__main__":
    if __name__ == '__main__':
        unittest.main()